---
layout: page
title: Media and Publications
permalink: /about/media-publications
---

## Publications:

[autohaem: 3D printed devices for automated preparation of blood smears](https://doi.org/10.1063/5.0076901), S. McDermott, J. Kim, A. Leledaki, D. Parry, L. Lee, A. Kabla, C. Mkindi, R. Bowman, P. Cicuta, Review of Scientific Instruments 93, 014104 (2022) [(arXiv)](https://arxiv.org/abs/2112.05631)

## Media:

[Automating Blood Smears](https://www.thenakedscientists.com/podcasts/short/automating-blood-smears), The Naked Scientists (Podcast) (2022)  
[Automating Blood Smears](https://www.thenakedscientists.com/articles/science-news/automating-blood-smears), The Naked Scientists (2022)  
[Automating Blood Smears for Easier Malaria Diagnosis](https://publishing.aip.org/publications/latest-content/automating-blood-smears-for-easier-malaria-diagnosis/), AIP Press release (2022)
