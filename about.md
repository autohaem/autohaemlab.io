---
layout: page
title: About
permalink: /about/
---

This is the website for autohaem, the open source design group for low cost automation of blood smears and stains.

We are proud to be members of the [Center for Global Equality cultivator](https://centreforglobalequality.org/)

## People

### University of Cambridge, UK

#### Core team

[Samuel McDermott](https://www.bss.phy.cam.ac.uk/directory/sjm263)  
[Pietro Cicuta](https://www.bss.phy.cam.ac.uk/directory/pc245)  

#### Project students

Erez Li  
Henry Dai  
Jathavan Thevarajah  
Joshua Abu  
_Check out their [report](https://erezli.gitlab.io/gm2-autohaem/) on the project._

Jaehyeon Kim

Anna-Katerina Leledaki  
Duncan Parry  
Louis Lee  
_Check out their [report](https://dfp24.gitlab.io/gm2-autohaem-malaria/) on the project._

#### Advisors

[Lara Allen]()  
[Alexandre Kabla](http://www.eng.cam.ac.uk/profiles/ajk61)  
[Guilherme Nettesheim](https://www.bss.phy.cam.ac.uk/directory/gn299)  
[Viola Introini](https://www.bss.phy.cam.ac.uk/directory/vi211)  

### University of Bath, UK

[Richard Bowman](https://researchportal.bath.ac.uk/en/persons/richard-bowman)

### Ifakara Health Institute, Tanzania  

Catherine Mkindi

## [Development i-Teams](https://iteamsonline.org/project-1/)

![](assets/development_i-teams.jpg)
Aracely Castillo-Venzor  
Iona Cuthbertson  
Jenny Fan  
Yuthika Pillai  
Xinyu Wang  
Marco Laub  
Mentor: Bill Mathews  
Program organiser: Amy Weatherup  
